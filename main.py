import numpy as np
import math
import sys
import random
import matplotlib.pyplot as plt
lines = np.loadtxt("database.cnf",dtype=int, delimiter="  ", unpack=False)


class agent:

    def __init__(self,modeled_env,method='hill_climbing',iteration_RRHK=0,annealing_rate=0.005):
        self.modeled_env=modeled_env
        self.alg=eval('self.'+method)
        self.iteration=iteration_RRHK
        self.annealing_rate=annealing_rate
        self.start_tmp=0

    def act(self):
        return self.alg()

    # 1
    def hill_climbing(self):
        current=self.modeled_env.state
        while True:
            next =self.modeled_env.successor_function(current[0])
            if next[1]<current[1]:
                current=next
            else:
                return current

    # 2
    def random_restart_hill_climbing(self):
        best=(0,sys.maxsize)
        for i in range(self.iteration):
            result=self.hill_climbing()
            print(result)
            if result[1]<best[1]:
                best=result
            self.modeled_env.set_random_state()
        return best

    # 3
    def simulated_annealing(self):
        history=[]
        current = self.modeled_env.state
        print(current)
        time=1
        self.start_tmp = self.schedule(time)
        while True:
            temperature=self.schedule(time)
            print("{:.3f}".format((self.start_tmp-temperature)*100/self.start_tmp),'%','conflicts: ',current[1])
            if temperature<0.0001:
                return current,history
            next=self.modeled_env.random_successor(current[0])
            if next[1]==0:
                history.append((time,0))
                return next,history
            delta_e=next[1]-current[1]
            if delta_e<0:
                current=next
            else:
                if random.random()<= math.exp(-delta_e/temperature):
                    current = next
            history.append((time,current[1]))
            time+=1



    def schedule(self,time):
        return 10000/math.exp(self.annealing_rate*time)



class problem:

    def __init__(self,cnf,code_length=100):
        self.cnf = cnf.ravel()
        tmp=[int(x) for x in np.floor(2 * np.random.random(code_length))]
        self.state=(tmp,self.claus_conflict_amount(tmp))
        self.code_length=code_length

    def successor_function(self,state):
        neighbors=[]
        values=[]
        for idx in range(len(state)):
            neighbor=[x for x in state]
            neighbor[idx]=0 if neighbor[idx] == 1 else 1
            conflicts=self.claus_conflict_amount(neighbor)
            neighbors.append(neighbor)
            values.append(conflicts)
        min_value = min(values)
        return neighbors[values.index(min_value)], min_value

    def random_successor(self,state):
        idx=random.randint(0,self.code_length-1)
        neighbor = [x for x in state]
        neighbor[idx] = 0 if neighbor[idx] == 1 else 1
        conflicts = self.claus_conflict_amount(neighbor)
        return neighbor,conflicts

    def claus_conflict_amount(self,variables):
        conflicts=0
        claus=0
        for idx in range(len(self.cnf)):
            if self.cnf[idx]==0:
                if claus==0:
                    conflicts+=1
                claus=0
            else:
                if self.cnf[idx]<0:
                    tmp=variables[-self.cnf[idx] - 1]
                    claus=claus | 0 if tmp==1 else 1
                else:
                    claus=claus | variables[self.cnf[idx] - 1]
        return conflicts

    def set_random_state(self):
        tmp = [int(x) for x in np.floor(2 * np.random.random(self.code_length))]
        self.state = (tmp, self.claus_conflict_amount(tmp))


def plot_history(history):
    x=[]
    y=[]
    for i in range(len(history)):
        x.append(history[i][0])
        y.append(history[i][1])
    plt.plot(x,y)
    plt.show()

# #####################code########################



# # hill_climbing
# print('hill_climbing method:\n')
# env=problem(lines)
# agnt=agent(env,method='hill_climbing')
# print('max,value : ',agnt.act())

# # random_restart_hill_climbing
# print('random_restart_hill_climbing method:\n')
# env=problem(lines)
# agnt=agent(env,method='random_restart_hill_climbing',iteration_RRHK=16)
# print('max,value : ',agnt.act())

# simulated annealing
print('simulated annealing method:\n')
env=problem(lines)
agnt=agent(env,method='simulated_annealing',annealing_rate=0.001)
best,hst=agnt.act()
print('result,value : ',best)
plot_history(hst)



